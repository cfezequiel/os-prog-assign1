#! /usr/bin/make

SDIR=src
IDIR=src
BDIR=bin
UDIR=build
TDIR=test/test_data
ODIR=test/output
VPATH=$(SDIR)

# Adjust compiler option based on OS
OS=$(shell uname)
ifeq ($(OS), Linux)
	THREAD_ARG=-pthread
else ifeq ($(OS), SunOS)
	THREAD_ARG=-pthreads
endif

ARGS=-I$(IDIR) -pedantic -Wall $(THREAD_ARG)
    
all: $(BDIR)/tfib test

$(BDIR)/tfib: fib.c array.c main.c 
	gcc $^ -o $@ $(ARGS) $(OPT_ARGS)

test:
	cd $(UDIR) && ./gen_test_data.py && cd ..

clean:
	rm -rf $(BDIR)/tfib

clean_all: clean
	rm -rf $(TDIR) $(ODIR)

.PHONY: clean clean_all test
