/* \file array.c
 * \author Carlos Ezequiel
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "array.h"


ARR_slice_t* ARR_split(int *array, int size, int count)
{
    int i;
    int index;
    int numElements;
    int extraElements;
    ARR_slice_t *out;

    assert(size >= count);

    out = malloc(sizeof(ARR_slice_t) * count);

    numElements = size / count;
    extraElements = size % count;

    /* Get the size for each slice */
    for (i = 0; i < count; i++)
    {
        out[i].size = numElements;
    }

    i = 0;
    while (extraElements--)
    {
        out[i % count].size++;
        i++;
    }

    /* Get the array for each slice */
    index = 0;
    for (i = 0; i < count; i++)
    {
        out[i].array = array + index;
        index += out[i].size;
    }

    return out;
}

void ARR_printArray(int *array, int size)
{
    int i;

    printf("[");
    for (i = 0; i < size; i++)
    {
        if (i != size - 1)
        {
            printf("%d, ", array[i]);
        }
        else
        {
            printf("%d]", array[i]);
        }
    }
}


