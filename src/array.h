/* \file array.h
 * \author Carlos Ezequiel
 */

typedef struct
{
    int *array;
    int size;
} ARR_slice_t;

ARR_slice_t* ARR_split(int *array, int size, int count);
void ARR_printArray(int *array, int size);

