/* \file fib.c
 * \author Carlos Ezequiel
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#include "fib.h"
#include "array.h"

/**
  * Recursive fibonacci function
  */
int FIB_computeFibonacciRecursive(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (n == 1)
    {
        return 1;
    }
    else
    {
        return FIB_computeFibonacciRecursive(n - 1) +
            FIB_computeFibonacciRecursive(n - 2);
    }
}

/**
  * Non-recursive fibonacci function.
  * Reference: http://forum.codecall.net/c-c/8594-fibonacci-no-recursion-fun.html
  */
int FIB_computeFibonacci(int n)
{
	int prev = -1;
	int result = 1;
	int sum;
	int i;
	
	for(i = 0;i <= n;++ i)
	{
		sum = result + prev;
		prev = result;
		result = sum;
	}
	
	return result;
}

void FIB_computeFibonacciMany(int *output, int *input, int size)
{
    int i;

    for (i = 0; i < size; i++)
    {
#if 0
        output[i] = FIB_computeFibonacciRecursive(input[i]);
#else /* Use non-recursive instead */
        output[i] = FIB_computeFibonacci(input[i]);
#endif

    }
}

void *FIB_computeFibonacciThreaded(void *s)
{
    ARR_slice_t *slice = (ARR_slice_t *) s;
    int size = slice->size;
    int *array = slice->array;

#ifdef DEBUG
    printf("Processing array:");
    ARR_printArray(array, size);
    printf("\n");
#endif

    FIB_computeFibonacciMany(array, array, size);

    return NULL;
}

int FIB_computeFibonacciManyThreaded(int *output, int *input, int size,
        int threadCount)
{
    int i;
    int rc;
    pthread_t *threads;
    ARR_slice_t *slices;

    /* Create thread pool */
    threads = malloc(sizeof(pthread_t) * threadCount);

    /* Copy input to output */
    memcpy(output, input, sizeof(int) * size);

    /* 
     * If there are more threads than data elements,
     * use only as much threads as data elements
     */
    if (size < threadCount)
    {
        threadCount = size;
    }

    /* Divide the data for given number of threads */
    slices = ARR_split(output, size, threadCount);

    /* Do the work */
    for (i = 0; i < threadCount; i++)
    {
#ifdef DEBUG
        printf("Creating thread #%d\n", i);
#endif
        rc = pthread_create(
                    &threads[i], 
                    NULL, 
                    FIB_computeFibonacciThreaded,
                    (void *) &slices[i]
                );
        if (rc)
        {
            /* Error */
            return rc;
        }
    }

    /* Wait for all threads to complete */
    for (i = 0; i < threadCount; i++)
    {
        pthread_join(threads[i], NULL);
    }

    /* Release allocated memories */
    free(threads);
    free(slices);

    return 0;
}
