/* \file main.c 
 * \author Carlos Ezequiel
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#include "fib.h"

int readDataFromFile(FILE *fp, int *data, int size)
{
    int i = 0;
    int value;

    while (fscanf(fp, "%d, ", &value) > 0)
    {
        data[i++] = value;
        if (i >= size)
        {
            break;
        }
    }

    return i;
}

void printMatrix(const int *data, int order)
{
    int i;
    int n = order * order;

    for (i = 0; i < n; i++)
    {
        printf("%d, ", data[i]);
        if (i % order == order - 1)
        {
            printf("\n");
        }
    }
}

int main(int argc, char *argv[])
{
    int res;
    int *input;
    FILE *fp;
    int threadCount;
    int order;
    int size;
    int *output;

    if (argc != 2)
    {
        printf("Please specify input file\n");
        return EXIT_FAILURE;
    }

    /* Open the file, check for errors */
    fp = fopen(argv[1], "r");
    if (fp == NULL)
    {
        printf("Could not open file.\n");
        return EXIT_FAILURE;
    }

    /* Read number of threads and matrix order */
    res = fscanf(fp, "%d %d ", &threadCount, &order);
    if (res != 2)
    {
        printf("Could not read file: Invalid format.\n");
        return EXIT_FAILURE;
    }

    /* Check parameters */
    if (threadCount <= 0)
    {
        printf("Invalid threadCount value: %d", threadCount);
        return EXIT_FAILURE;
    }
    if (order <= 0)
    {
        printf("Invalid matrix order value: %d", order);
        return EXIT_FAILURE;
    }

    /* Read matrix as array */
    size = order * order;
    input = malloc(sizeof(int) * size);
    res = readDataFromFile(fp, input, size);
    if (res < size)
    {
        printf("Number of values read not as expected.\n");
        return EXIT_FAILURE;
    }

    /* Close the file */
    fclose(fp);

    /* Compute fibonacci sum of all elements of the matrix */
    output = malloc(sizeof(int) * size);
    FIB_computeFibonacciManyThreaded(output, input, size, threadCount);

    /* Print the output matrix to the stdout */
    printMatrix(output, order);

    /* Free allocated memory */
    free(input);
    free(output);

    /* Close all threads */
    pthread_exit(NULL);

    return EXIT_SUCCESS;
}


