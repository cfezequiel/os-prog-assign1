/* \file fib.h
 * \author Carlos Ezequiel
 */

int FIB_computeFibonacciRecursive(int n);
int FIB_computeFibonacci(int n);
void FIB_computeFibonacciMany(int *output, int *input, int size);
int FIB_computeFibonacciManyThreaded(int *output, int *input, int size,
        int threadCount);
