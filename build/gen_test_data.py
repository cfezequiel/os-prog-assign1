#! /usr/bin/python

import random
import os

TEST_DIR = "../test"
TEST_DATA_DIR = TEST_DIR + "/test_data"

DEFAULT_MIN = 0
DEFAULT_MAX = 9

# Generate the test data

def createTestFile(filename, threadCount, order, minVal=DEFAULT_MIN,
        maxVal=DEFAULT_MAX):

    f = open(filename, 'w')
# Write thread count and matrix order
    f.write(str(threadCount) + '\n')
    f.write(str(order) + '\n')

    # Generate matrix data
    for i in range(order):
        line = [str(random.randint(minVal, maxVal)) for i in range(order)]
        f.write(", ".join(line) + ",\n")

    f.close()

if __name__ == '__main__':

    # Create test data directory, if it doesn't exist
    if not os.path.exists(TEST_DATA_DIR):
        os.makedirs(TEST_DATA_DIR)

    os.chdir(TEST_DATA_DIR)

    # ==== Functionality Tests ====
    # Simple
    createTestFile("test_thread_1.dat", 1, 3, 3, 3)
    createTestFile("test_thread_2.dat", 2, 3, 6, 6)

    # ==== Performance Tests ====
    # Test: Changing thread count
    maxThreadCount = 8
    basename = "perf_thread_"
    order = 1000
    for i in range(1, maxThreadCount + 1): 
        filename = basename + "%03d" % i + ".dat"
        createTestFile(filename, i, order, maxVal=100)

    # Test: Changing order
    threadCount = 8
    basename = "perf_order_"
    for i in range(1, 10): 
        order = i*200
        filename = basename + "%04d" % order + ".dat"
        createTestFile(filename, threadCount, order, maxVal=100)

    # Test: Changing maximum range
    # threadCount = 8
    order = 1000
    basename = "perf_range_"
    for i in range(1, 4): 
        maxVal = pow(10, i)
        filename = basename + "%04d" % maxVal + ".dat"
        createTestFile(filename, threadCount, order, maxVal=maxVal)


