#! /usr/bin/python

"""Test functionality of fibonacci program."""

import os
import sys
import commands

OUT_DIR = 'output'
TEST_DIR = 'test_data'
PROG = '../bin/tfib'

def getsh(cmd, show=False):
    if show:
        print cmd
    return commands.getoutput(cmd)

def fibonacci(n):
    """Fibonacci function."""

    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)

def genExpOutput(testFile):
    """Generate an expected output fibonacci matrix."""

    f = open(testFile, 'r')
    threadCount = int(f.readline())
    order = int(f.readline())
    matrix = [int(x) for x in f.read().replace(',', '').split()]
    f.close()

    out = []
    for num in matrix:
        out.append(fibonacci(num))

    return out

if __name__=='__main__':
    # Get test filenames
    filenames = sorted(os.listdir(TEST_DIR))

    for filename in filenames:
        # Ignore other types of tests
        if filename.find('test_') < 0:
            continue

        # Execute software-under-test
        path = TEST_DIR + "/" + filename
        cmd = PROG + " " + path
        output = [int(x) for x in getsh(cmd).replace(',', '').split()]

        # Generate expected output
        expOutput = genExpOutput(path)

        # Compare and report
        testName = os.path.splitext(filename)[0]
        if output != expOutput:
            print "%s: FAIL" % testName
            print "\toutput = %s" % output
            print "\texpected = %s" % expOutput
        else:
            print "%s: OK" % testName


