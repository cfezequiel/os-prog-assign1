#! /usr/bin/python

""" Performance test runner for fibonacci program. """

import os
import sys
import commands

OUT_DIR = 'output'
TEST_DIR = 'test_data'
PROG = '../bin/tfib'
TIME='/usr/bin/time -f "real=%e"'

def getsh(cmd, show=False):
    if show:
        print cmd
    return commands.getoutput(cmd)

def getTestInfo(filename):
    """Get no. of threads specified in test file."""

    f = open(filename, 'r')
    threadCount  = int(f.readline())
    order = int(f.readline())
    f.close()

    return (threadCount, order)

if __name__ == '__main__':

    # Collect all filenames from test directory
    filenames = sorted(os.listdir(TEST_DIR))

    # Create output directory, if it does not exist
    if not os.path.exists(OUT_DIR):
        os.makedirs(OUT_DIR)

    # Execute program for each filename
    header = "Test\t\tThreads\tOrder\tElapsedTime"
    print header
    results = ""
    for filename in filenames:
        # Skip non-performance test files
        if filename.find('perf_') < 0:
            continue

        path = TEST_DIR + "/" + filename
        (threadCount, order) = getTestInfo(path)
        output = getsh(TIME + " " + PROG + " " + path)
        (matrix, time) = output.split("real=")
        time = float(time)
        
        # Store result
        (rootName, ext) = os.path.splitext(filename)
        results += "%s\t%d\t%d\t%0.2f\n" % (rootName, threadCount, order, time)
    
        # Write matrix output as csv file
        csvFile = OUT_DIR + "/" + rootName + ".csv"
        f = open(csvFile, 'w')
        f.write(matrix)
        f.close()

    print results
    resultsFile = os.path.splitext(sys.argv[0])[0] + ".csv"
    f = open(resultsFile, "w")
    f.write(", ".join(header.split()) + '\n')
    f.write(results.replace('\t', ', '))
    f.close()


